const { i18n } = require('./next-i18next.config');

const configNext = {
  reactStrictMode: true,
  i18n,
  pageExtensions: ['page.ts', 'page.tsx'],
  future: {
    strictPostcssConfiguration: true
  },
  images: {
    domains: String(process.env.IMAGE_DOMAINS).split(',')
  },
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/home'
      }
    ];
  },
  generateBuildId: () => nextBuildId({ dir: __dirname })
}

module.exports = configNext;

