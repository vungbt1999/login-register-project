const path = require('path');

module.exports = {
  i18n: {
    defaultLocale: process.env.NEXT_PUBLIC_LOCALE_DEFAULT || 'vi',
    locales: ['vi', 'en'],
    defaultNS: 'common',
    localePath: path.resolve('./src/res/translations')
  }
};
