import React from 'react'
import styles from './styles.module.scss';


export default function FooterLayout() {
    return (
        <div className={styles.wrapperFooter}>
            <span>
                create by @vungbt1999 (
                    <a
                        className={styles.repo}
                        href="https://gitlab.com/vungbt1999"
                        target="_blank"
                        rel="noreferrer">
                        repo
                    </a>
                )
            </span>
        </div>
    )
}
