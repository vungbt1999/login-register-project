import { useRouter } from 'next/router';
import OGMeta from 'libraries/head/OGMeta';
import SEOMeta from 'libraries/head/SEOMeta';
import MenuLayout from 'libraries/layouts/menu/MenuLayout';
import FooterLayout from 'libraries/layouts/footer/FooterLayout';
import Loading from 'libraries/loading/Loading';

type Props = {
    children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
}

export default function MainLayout({ children }: Props) {
    const router = useRouter();
    return (
        <>
            <SEOMeta/>
            <OGMeta />
            <MenuLayout />
            {router.isFallback ? (
                <Loading />
            ) : (
                children
            )}
            <FooterLayout />
        </>
    )
}
