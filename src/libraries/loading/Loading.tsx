import { SunIcon } from '@heroicons/react/outline';

type Props = {
    loading?: boolean;
    className?: string;
};

const Loading = (props: Props) => {
    const { loading, className } = props;
    return <>{loading !== false && <SunIcon className={`${className}`} />}</>;
};

export default Loading;
