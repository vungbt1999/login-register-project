import Head from 'next/head';
import React from 'react';

type OGMetaProps = {
  title?: string;
  type?: string;
  url?: string;
  image?: string;
  description?: string;
  siteName?: string;
};

export default function OGMeta(props: OGMetaProps) {
  const {
    title = process.env.NEXT_PUBLIC_APP_NAME,
    type,
    url,
    image,
    description,
    siteName = process.env.NEXT_PUBLIC_APP_NAME
  } = props;
  return (
    <Head>
      <meta property="og:title" content={title} />
      <meta property="og:type" content={type || 'article'} />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={image} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content={siteName} />
    </Head>
  );
}
