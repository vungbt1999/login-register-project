import { merge } from 'lodash';
import { GetServerSideProps } from 'next';
import { getSession } from 'next-auth/client';
import { stringify } from 'query-string';

// only getServerSideProps
export function withSSAuth(cb?: GetServerSideProps) {
    return async (ctx: any) => {
        const session = await getSession(ctx);
        if (!session) {
            return {
                redirect: {
                    destination: `/auth/login?${stringify({ callbackUrl: ctx.resolvedUrl })}`,
                    permanent: false
                }
            };
        }
        return merge(cb ? await cb(ctx) : {}, { props: { session } });
    };
}
