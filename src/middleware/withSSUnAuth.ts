import { GetServerSideProps } from 'next';
import { getSession } from 'next-auth/client';

// only getServerSideProps
export function withSSUnAuth(cb?: GetServerSideProps) {
  return async (context: any) => {
    const session = await getSession(context);
    if (session) {
      return {
        redirect: {
          destination: String(context.query?.callbackUrl || '/'),
          permanent: false
        }
      };
    }
    return cb ? await cb(context) : { props: {} };
  };
}
