import { configureStore, ConfigureStoreOptions, getDefaultMiddleware } from '@reduxjs/toolkit';
// import locale from 'hooks/base/locale/reducer';
// import nprogress from 'hooks/base/nprogress/reducer';
import ui from 'hooks/ui/reducer';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';

const rootReducer = {
  // locale,
  // nprogress,
  ui
};

const isClient = typeof window !== 'undefined';

const options: ConfigureStoreOptions = {
  reducer: rootReducer
};

options.middleware = getDefaultMiddleware();
if (isClient) {
  options.middleware = options.middleware.concat([
    createStateSyncMiddleware({
      whitelist: ['locale/changeLocale', 'ui/changeUI']
    })
  ]);
}

const store = configureStore(options);

if (isClient) {
  initMessageListener(store);
}

export type RootState = ReturnType<typeof store.getState>;
export default store;
