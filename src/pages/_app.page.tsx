import Head from 'next/head';
import { Provider } from 'react-redux';
import type { AppProps } from 'next/app';
import store from 'utils/reduxStore';
import 'styles/main.scss';
import { appWithTranslation } from 'next-i18next';

function Root({ children }: any) {
    // useInitRoot();
    return (
        <>
            <Head>
                <meta name="viewport" content="width=device-width,initial-scale=1" />
            </Head>
            { children }
            {/* <ChangeTheme /> */}
        </>
    );
}

function MyApp({ Component, pageProps }: AppProps) {
    const Layout = (Component as any).Layout || (({ children }: any) => <>{children}</>);
    return (
        <Provider store={store}>
            <Root>
                <Layout>
                    <Component {...pageProps} />
                </Layout>
            </Root>
        </Provider>
    );
}

export default appWithTranslation(MyApp);
