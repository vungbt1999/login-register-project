import React, { useEffect, useState } from 'react'
import styles from './styles.module.scss';
import { useTranslation } from 'next-i18next'
import { withTranslations } from 'middleware/withSSTranslations';
import Link from 'next/link';

function LoginPage() {
    const { t } = useTranslation();
    
    useEffect(() => {
        if(document === undefined) return;
        let wrapperLogin = document.getElementById('loginWrapper');
        if(wrapperLogin) {
            setTimeout(() => {
                wrapperLogin?.classList.add(styles.showSignIn);
            }, 200)
        }
    }, []);


    const onShowSignUp = () => {
        if(document === undefined) return;
        let wrapperLogin = document.getElementById('loginWrapper');
        if(wrapperLogin) {
            wrapperLogin?.classList.remove(styles.showSignIn);
            wrapperLogin?.classList.add(styles.showSignUp);
        }
    }

    const onShowSignIn = () => {
        if(document === undefined) return;
        let wrapperLogin = document.getElementById('loginWrapper');
        if(wrapperLogin) {
            wrapperLogin?.classList.add(styles.showSignIn);
            wrapperLogin?.classList.remove(styles.showSignUp);
        }
    }

    return (
        <div className={styles.loginWrapper} id="loginWrapper">
            {/** Register Form */}
            <div className={`center-item ${styles.leftContent}`}>
                <div className={styles.formLogin}>
                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icUser.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.username')}/>
                    </div>

                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icEmail.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.email')}/>
                    </div>

                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icUser.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.password')}/>
                    </div>

                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icLock.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.confirmPassword')}/>
                    </div>

                    <button className={styles.btnSignIn}>{t('auth.signUp')}</button>

                    <p className={styles.havAccount}>
                        {t('auth.youHaveAccount')}
                        <span className={styles.toggleRegister} onClick={onShowSignIn}>
                            {t('auth.signIn')}
                        </span>
                    </p>
                </div>

                {/** Social Form */}
                <div className={styles.wrapperSocial}>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icFacebook.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icGoogle.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icTwitter.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icInsta.svg" alt="social"/>
                    </div>
                </div>
            </div>

            {/** Login Form */}
            <div className={`center-item ${styles.rightContent}`}>
                <div className={styles.formLogin}>
                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icUser.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.username')}/>
                    </div>

                    <div className={styles.inputGroup}>
                        <img className={styles.iconInput} src="/icons/auth/icLock.svg" alt="user input" />
                        <input className={styles.input} placeholder={t('auth.password')}/>
                    </div>
                    <button className={styles.btnSignIn}>{t('auth.signIn')}</button>
                    <p className={styles.forgotPassword}>
                        <Link href='/forgot-password'>
                            <a className={styles.link}>
                                {t('auth.forgotPassword')}
                            </a>
                        </Link>
                    </p>
                    <p className={styles.havAccount}>
                        {t('auth.dontHaveAccount')}
                        <span className={styles.toggleRegister} onClick={onShowSignUp}>
                            {t('auth.signUp')}
                        </span>
                    </p>
                </div>

                {/** Social Form */}
                <div className={styles.wrapperSocial}>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icFacebook.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icGoogle.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icTwitter.svg" alt="social"/>
                    </div>
                    <div className={`center-item ${styles.socialItem}`}>
                        <img className={styles.icon} src="/icons/auth/icInsta.svg" alt="social"/>
                    </div>
                </div>
            </div>

            {/** Content */}
            <div className={styles.backgroundContent}>
                {/** Content Login */}
                <div className={`center-item ${styles.leftContent}`}>
                    <div className={`${styles.messContent} ${styles.hiddenSignIn}`}>
                        <h2 className={styles.welcome}>
                            Welcome back
                        </h2>
                        <p className={styles.description}>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit obcaecati, accusantium
                            molestias, laborum, aspernatur deserunt officia voluptatum tempora dicta quo ab ullam. Assumenda
                            enim harum minima possimus dignissimos deserunt rem.
                        </p>
                    </div>
                    <div className={`${styles.unDrawLoginView} ${styles.hiddenSignIn}`}>
                        <img className={styles.unDrawLogin} src="/unDraw/undraw_login.svg" alt="undraw login"/>
                    </div>
                </div>

                {/** Content Register */}
                <div className={`center-item ${styles.rightContent}`}>
                    <div className={`${styles.unDrawLoginView} ${styles.hiddenSignIn}`}>
                        <img className={styles.unDrawLogin} src="/unDraw/undraw_register.svg" alt="undraw login"/>
                    </div>
                    <div className={`${styles.messContent} ${styles.hiddenSignIn}`}>
                        <h2 className={styles.welcome}>
                            Join With Us
                        </h2>
                        <p className={styles.description}>
                            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Impedit obcaecati, accusantium
                            molestias, laborum, aspernatur deserunt officia voluptatum tempora dicta quo ab ullam. Assumenda
                            enim harum minima possimus dignissimos deserunt rem.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export const getServerSideProps = withTranslations();

export default LoginPage;

