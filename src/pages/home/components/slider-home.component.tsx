import { size } from 'lodash';
import { useState } from 'react';
import styles from './styles.module.scss';

enum TypeColor {
    RED = 'RED',
    PINK = 'PINK',
    YELLOW = 'YELLOW',
    BLACK = 'BLACK',
    SLIVER = 'SLIVER',
    GREEN = 'GREEN',
    WHITE = 'WHITE',
    BLUE = 'BLUE'
}

const LIST_PRODUCT = [
    {
        id: 1,
        image: "images/zoomx-vaporfly-next-running-shoe-4Q5jfG.png",
        moreImage: [
            "/images/zoomx-vaporfly-next-running-shoe-4Q5jfG-1.jpg",
            "/images/zoomx-vaporfly-next-running-shoe-4Q5jfG (1).jpg",
            "/images/zoomx-vaporfly-next-running-shoe-4Q5jfG (2).jpg",
            "/images/zoomx-vaporfly-next-running-shoe-4Q5jfG (3).jpg",
        ],
        name: "Nike ZoomX Vaporfly NEXT%",
        price: 230,
        sizes: [
            "37",
            "38",
            "39",
            "40",
            "41",
            "42"
        ],
        colors: [
            "GREEN",
            "PINK",
            "YELLOW",
            "BLACK"
        ],
        description: "The Nike ZoomX Vaporfly NEXT% clears your path to record-breaking speed with a lighter design and faster feel than before. With more cushioning underfoot and reduced weight up top, the result is unprecedented energy return and comfort"
    },
    {
        id: 2,
        image: "images/zoom-fly-3-mens-running-shoe-XhzpPH.png",
        moreImage: [
            "/images/zoom-fly-3-mens-running-shoe-XhzpPH.jpg",
            "/images/zoom-fly-3-mens-running-shoe-XhzpPH (1).jpg",
            "/images/zoom-fly-3-mens-running-shoe-XhzpPH (2).jpg",
            "/images/zoom-fly-3-mens-running-shoe-XhzpPH (3).jpg",
        ],
        name: "Nike Zoom Fly 3",
        price: 240,
        sizes: [
            "37",
            "38",
            "39",
            "40",
            "41",
        ],
        colors: [
            "PINK",
            "BLACK",
            "SLIVER"
        ],
        description: "The Nike ZoomX Vaporfly NEXT% clears your path to record-breaking speed with a lighter design and faster feel than before. With more cushioning underfoot and reduced weight up top, the result is unprecedented energy return and comfort"
    },
    {
        id: 3,
        image: "images/air-max-alpha-tr-3-mens-training-shoe-0C1CV7.png",
        moreImage: [
            "/images/air-max-alpha-tr-3-mens-training-shoe-0C1CV7.jpg",
            "/images/air-max-alpha-tr-3-mens-training-shoe-0C1CV7 (1).jpg",
            "/images/air-max-alpha-tr-3-mens-training-shoe-0C1CV7 (2).jpg",
            "/images/air-max-alpha-tr-3-mens-training-shoe-0C1CV7 (3).jpg",
        ],
        name: "Nike Air Max Alpha TR 3",
        price: 250,
        sizes: [
            "37",
            "38",
            "40",
            "41",
        ],
        colors: [
            "PINK",
            "BLACK",
            "SLIVER",
            "GREEN"
        ],
        description: "The Nike ZoomX Vaporfly NEXT% clears your path to record-breaking speed with a lighter design and faster feel than before. With more cushioning underfoot and reduced weight up top, the result is unprecedented energy return and comfort"
    },
    {
        id: 4,
        image: "images/air-zoom-superrep-mens-hiit-class-shoe-ZWLnJW (1).png",
        moreImage: [
            "/images/air-zoom-superrep-mens-hiit-class-shoe-ZWLnJW.jpg",
            "/images/air-zoom-superrep-mens-hiit-class-shoe-ZWLnJW (4).jpg",
            "/images/air-zoom-superrep-mens-hiit-class-shoe-ZWLnJW (3).jpg",
            "/images/air-zoom-superrep-mens-hiit-class-shoe-ZWLnJW (2).jpg",
        ],
        name: "Nike Air Max Alpha TR 3",
        price: 260,
        sizes: [
            "37",
            "38",
            "40",
            "41",
            "42"
        ],
        colors: [
            "PINK",
            "BLACK",
            "GREEN"
        ],
        description: "The Nike ZoomX Vaporfly NEXT% clears your path to record-breaking speed with a lighter design and faster feel than before. With more cushioning underfoot and reduced weight up top, the result is unprecedented energy return and comfort"
    }
]


export default function SliderHomeComponent() {

    const renderColor = (color: string) => {
        let colorStyle = "default-color";
        
        switch(color) {
            case TypeColor.RED: 
                colorStyle = "red-color";
                break;
            case TypeColor.PINK: 
                colorStyle = "pink-color";
                break;
            case TypeColor.YELLOW: 
                colorStyle = "yellow-color";
                break;
            case TypeColor.BLACK: 
                colorStyle = "black-color";
                break;
            case TypeColor.SLIVER: 
                colorStyle = "sliver-color";
                break;
            case TypeColor.GREEN: 
                colorStyle = "green-color";
                break;
            case TypeColor.WHITE: 
                colorStyle = "white-color";
                break;
            case TypeColor.BLUE: 
                colorStyle = "blue-color";
                break;
        }
        return colorStyle;
    }

    const [idProduct, setIdProduct] = useState<number>(1);
    const [indexProduct, setIndexProduct] = useState<number>(0);

    const onClickController = async (id: number, index: number) => {
        setTimeout(() => {
            setIndexProduct(index);
        }, 1000);
        // setIndexProduct(index);
        setIdProduct(id);
    }


    return (
        <div className={styles.wrapperSlider}>
            <div className={styles.slider} style={{marginTop: `-${indexProduct}00vh`}}>
                {LIST_PRODUCT.map((item, index) => {
                    const { price, name, colors, sizes, description, image, moreImage} = item;
                    return (
                        <div className={`${styles.wrapperSlide} ${idProduct === item.id ? styles.activeSlide : ""}`} key={index}>
                            <div className={`${styles.leftContent}`}>
                                {/** product info */}
                                <div className={styles.productInfo}>
                                    <div className={styles.infoWrapper}>
                                        {/** product price */}
                                        <div className={styles.price}>
                                            <span>$</span>{price}
                                        </div>

                                        {/** product name */}
                                        <div className={styles.name}>
                                            <h1>{name}</h1>
                                        </div>
                                        {/** product size */}
                                        <div className={styles.size}>
                                            <p className={styles.title}>SIZE</p>
                                            <div className={styles.sizeWrapper}>
                                                {sizes.map((size, iSize) => {
                                                    return (
                                                        <div className={styles.sizeItem} key={iSize}>{size}</div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        {/** product color */}
                                        <div className={styles.color}>
                                            <p className={styles.title}>COLOR</p>
                                            <div className={styles.colorWrapper}>
                                                {colors.map((color, iColor) => {
                                                    return (
                                                        <div className={styles.colorItem} key={iColor}>
                                                            <div className={`${styles.typeColor} ${renderColor(color)}`}></div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                        {/** product description */}
                                        <p className={styles.description}>{description}</p>
                                        <div className={styles.btnAddCartView}>
                                            <button className={styles.btnAddCart}>
                                                Thêm vào giỏ hàng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={`${styles.rightContent} ${styles.backgroundProduct}`}>
                                {/** main product */}
                                <div className={styles.productImage}>
                                    <div className={styles.imageWrapper}>
                                        <img className={styles.image} src={image} alt="placeholder+image"/>
                                    </div>
                                </div>

                                {/** more product */}
                                <div className={styles.listMoreImage}>
                                    {moreImage.map((imgItem, iImage) => {
                                        return (
                                            <div className={styles.moreImagesItem} key={iImage}>
                                                <img className={styles.image} src={imgItem} alt="placeholder+image"/>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    )
                })}
                
                {/** slider controller */}
                <div className={styles.sliderController}>
                    {LIST_PRODUCT.map((item, index) => {
                        return (
                            <div className={`${styles.itemController} ${idProduct === item.id ? styles.activeController : ''}`} key={index} onClick={() => onClickController(item.id, index)}>
                                <img className={styles.image} src={item.image} alt="placeholder+image"/>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}