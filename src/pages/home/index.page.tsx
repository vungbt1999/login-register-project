import MainLayout from 'libraries/layouts/MainLayout';
import { withTranslations } from 'middleware/withSSTranslations';
import { useTranslation } from 'next-i18next';
import styles from './styles.module.scss';
import SEOMeta from 'libraries/head/SEOMeta';
import SliderHomeComponent from './components/slider-home.component';

function HomePage() {
    const { t } = useTranslation();
    return (
        <div className={styles.homeWrapper}>
            <SEOMeta titleSuffix={t('home')} />
            <SliderHomeComponent />
        </div>
    )
}

HomePage.Layout = MainLayout;
export const getServerSideProps = withTranslations();

export default HomePage;