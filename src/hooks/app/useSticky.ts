import { useEffect, useState } from "react";

interface Utils {
    isSticky: boolean;
}

function useSticky(): Utils {

    const [isSticky, setSticky] = useState(false)

    const handleScroll = () => {
        if (window.pageYOffset > 80) {
            if(!isSticky){
                setSticky(true);
            }
        }else{
            if(isSticky){
                setSticky(true);
            }
        }
        setSticky(window.scrollY >= 84);
    }

    useEffect(() => {
        window.addEventListener("scroll",handleScroll)
        return () => {
            window.removeEventListener("scroll", () => handleScroll)
        }
    }, []);

    return {
        isSticky
    }
}

export default useSticky;