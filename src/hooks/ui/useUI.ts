import { RootState } from 'utils/reduxStore';
import { useDispatch, useSelector } from 'react-redux';
import { changeUI } from './reducer';

export default function useUI() {
  const ui = useSelector((state: RootState) => state.ui);
  const dispatch = useDispatch();
  const setUI = (state: RootState) => {
    const action = changeUI({ ...state });
    dispatch(action);
  };
  return { ui, setUI };
}
