import useInitUI from './useInitUI';

export default function useInitRoot() {
  useInitUI();
}
