import localStorageHelper, { KeyStorage } from 'utils/localStorage';
import { useEffect } from 'react';
import useUI from './useUI';

function useInitUI() {
  const { ui } = useUI();
  useEffect(() => {
    if (typeof window !== 'undefined' && ui.theme) {
      localStorageHelper.setObject(KeyStorage.UI, ui);
    }
  }, [ui]);

  useEffect(() => {
    if (typeof window !== 'undefined' && ui.theme) {
      const root = document.documentElement;
      Object.keys(ui.color).forEach((property) => {
        root.style.setProperty(`--color-${property}`, ui.color[property]);
      });
    }
  }, [ui.color]);

  useEffect(() => {
    if (typeof window !== 'undefined' && ui.theme) {
      document.documentElement.className = ui.theme || 'light';
    }
  }, [ui.theme]);

  useEffect(() => {
    const setHeightScreen = () => {
      if (window) {
        const root = document.documentElement;
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        const vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        root.style.setProperty('--height-screen', `calc(${vh}px * 100)`);
      }
    };
    setHeightScreen();
    // We listen to the resize event
    window && window.addEventListener('resize', setHeightScreen);
    return () => window && window.removeEventListener('resize', setHeightScreen);
  }, []);
}

export default useInitUI;
