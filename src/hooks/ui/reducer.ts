import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from 'utils/localStorage';
import _ from 'lodash';

type ColorTheme = {
  primary: string;
};

type UIState = {
  theme: 'light' | 'dark';
  color: ColorTheme;
};

const initialState: UIState = localStorageHelper.getObject(KeyStorage.UI, {
  theme: 'light',
  color: {
    primary: '#0d6efd'
  }
}) as UIState;

const page = createSlice({
  name: 'ui',
  initialState: initialState,
  reducers: {
    changeUI: (state: UIState, action: PayloadAction<UIState>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = page;
export const { changeUI } = actions;
export default reducer;
